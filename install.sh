#!/usr/bin/env bash

## generate the requirements.txt
./gen_requirements.sh

## install the modules from the requirements.txt
python3 -m pip install -r requirements.txt

## use zipapp to create one single python executable from the whole src
python3 -m zipapp src -p "/usr/bin/env python3" -o dist/mc2

## copy the final executable to /usr/bin
sudo cp dist/mc2 /usr/bin/mc2

## make it executable by anyone
sudo chmod +x /usr/bin/mc2

## finished installing
echo "[+] mc2 installed to /usr/bin/mc2"
